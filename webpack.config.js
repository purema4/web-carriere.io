//const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
//const dev = process.env.NODE_ENV !== "production" 
const path = require('path')
const webpack = require('webpack')

let config = {

    entry: './js/main.js',
    output: {
        filename: 'main.min.js',
        path: path.resolve(__dirname, 'js'),
        publicPath: "/js"
    },
    resolve: {
        extensions: ['.js']
    },

    module: {
        rules: [{     
            test:/\.js$/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ["babel-preset-env"]
                }                
            },
            exclude: /node_modules/
        }]
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        })
    ]
}

if(process.env.NODE_ENV === 'production') {
    config.plugins.push (
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    )
}

module.exports = config